import React, { useContext, useState, useEffect } from "react";
import firebase from "firebase/app";
//When a use require it works, not with import.
const urlSlug = require("url-slug");
import { NoticeArea } from "../components/Notice";

//data
import AppContext from "../components/AppContext";
import Upload from "../components/Upload";
import { ProductInterface } from "../api/Product";
import { getHomeCards } from '../api/Card';

//icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons";
import { withRouter } from "react-router";
import { loadProfile } from "../api/Profile";

/**
 * PRODUCT CREATE
 * 
 */
const ProductCreate = ({ history, location }: any) => {

  // Main controller.
  const { addNotice } = useContext(AppContext);

  // Data of a product.
  const [data, setData] = useState<ProductInterface | any>({});

  //Data of the authors
  const [profileAuthor, setProfileAuthor] = useState("")

  // Adress File
  const [fileAdress, setFileAdress] = useState("");

  // Adress File
  const [fileStoragePath, setFileStoragePath] = useState("image/products/");


  /**
   * Save the product
   */
  function handleSave() {

    const date = new Date();
    firebase
      .firestore()
      .collection("products")
      .doc(urlSlug(data.name))
      .set({
        date: date.getDate() +
          "/" +
          ("0" + (date.getMonth() + 1)).slice(-2) +
          "/" +
          date.getFullYear() +
          "  " +
          ("0" + date.getHours()).slice(-2) +
          ":" +
          ("0" + date.getMinutes()).slice(-2),
        timestamp: Date.now(),
        description: data.description || '',
        name: data.name,
        image: fileAdress,
        price: Number(data.price) || 0,
        stock: Number(data.stock) || 0,
        author: profileAuthor,
        format: data.format || '',
        path: urlSlug(data.name),
        path_author: location.param1,
        priceSold: 0
      })
      .then(() => {
        addNotice(`Le fanzine ${data.name} a été créé`, "is-success");
        history.push(`/fanzine/${urlSlug(data.name)}`);
      })
      .catch(err => {
        addNotice(err.message, "is-danger")
        console.log(err)
      });

  }

  function uploadToServer(compressedFile, storagePath) {
    console.log("compressedFile", compressedFile)

    //If there is a file, get it.
    if (compressedFile) {
      var reader = new FileReader();

      reader.onloadend = function (evt) {
        var storageRef = firebase.storage().ref(`${storagePath}${urlSlug(compressedFile.name)}`);
        console.warn(compressedFile); // Watch Screenshot
        var uploadTask = storageRef.put(compressedFile);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
          function (snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
          }, function (error) {
            switch (error.code) {
              case 'storage/unauthorized':
                // User doesn't have permission to access the object
                break;

              case 'storage/canceled':
                // User canceled the upload
                break;

              case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                break;
            }
          }, function () {
            // Upload completed successfully, now we can get the download URL
            uploadTask.snapshot.ref.getDownloadURL()
              .then(function (downloadURL) {
                setFileAdress(downloadURL)
              })
              .catch((err) => console.log("Error", err))
          });
      }
      reader.onerror = function (e) {
        console.log("Failed file read: " + e.toString());
      };
      reader.readAsArrayBuffer(compressedFile);
    }
  }


  // When the values of the form changes

  function handleChange(e: any, author?: string, path?: string) {

    if (e.target.name) {
      setData({
        ...data,
        [e.target.name]: e.target.value
      });
    }

    /**
     * Manage Free price
     */
    // Get the checkbox
    var checkBox = document.getElementById("freePrice");
    // Get the output text
    var price = document.getElementById("price");

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true) {
      price.style.display = "none";
    } else {
      price.style.display = "block";
    }
  }

  // On component load, load its data from firestore.
  useEffect(() => {
    getHomeCards().then(cards => {
      var potentialAuthor = []
      Object.keys(cards).forEach(function (key) {
        if (cards[key].profile_path) {
          potentialAuthor.push(cards[key])
        }
      });
    })

    if(location.param1){
      loadProfile(location.param1)
      .then(data => {
        if (data) {
          console.log("pass")
          setProfileAuthor(data.name)
        } 
      })
    }else {
      history.push(`/introuvable`);
      console.log("pass pas")
    }

   
  }, []);

  return (
    <>

      <NoticeArea />
      <nav
        className="navbar navbar is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div className="navbar-start">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light" onClick={() => history.goBack()}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faArrowLeft} />
                  </span>
                </a>
              </div>
            </div>
          </div>
          <h1 className="subtitle dark name">New</h1>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-light is-success" onClick={handleSave}>
                  <span className="icon">
                    <FontAwesomeIcon icon={faSave} />
                  </span>
                  <span>Enregister</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <Upload
        action={uploadToServer}
        storagePath={fileStoragePath} />

      <section className="section">
        <div className="container" />
        <div className="field">
          <label className="label">Fanzine créé par : </label>
          <h2 className="is-size-6 author">{profileAuthor}</h2>
          <div className="control">
          </div>
        </div>
        <div className="field">
          <label className="label">Titre</label>
          <div className="control">
            <input
              className="input"
              name="name"
              type="text"
              required
              onChange={handleChange}
              placeholder="Titre du fanzine"
            />
          </div>
        </div>

        <div className="field">
          <label className="label">Description</label>
          <div className="control">
            <textarea
              className="textarea"
              name="description"
              onChange={handleChange}
              placeholder="Courte description du contenu"
            />
          </div>
        </div>
        <hr />
        <p>Renseignez le prix commission inclue ou cliquez sur "Prix libre" (sans commission)</p>
        <hr />
        <div className="field">
          <input id="freePrice" onChange={handleChange} type="checkbox" name="freePrice" className="switch is-large" />
          <label htmlFor="freePrice" className="label">Prix Libre</label>
        </div>
        <div className="field" id="price">
          <label className="label">Prix</label>
          <div className="control">
            <input
              className="input"
              name="price"
              type="number"
              step="0.01"
              onChange={handleChange}
              required
              placeholder="X €"
            />
          </div>
        </div>
        <hr />
        <div className="field">
          <label className="label">Stock</label>
          <div className="control">
            <input
              className="input"
              name="stock"
              type="number"
              onChange={handleChange}
              required
              placeholder="Nombre d'exemplaires recus lors de la création"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Format</label>
          <div className="control">
            <input
              className="input"
              name="format"
              type="text"
              onChange={handleChange}
              placeholder="A4, A3, A2..."
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default withRouter(ProductCreate);
