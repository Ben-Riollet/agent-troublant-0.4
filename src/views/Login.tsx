import React from 'react';

//Data
import { NoticeArea } from '../components/Notice';
import LoginForm from '../components/LoginForm';
import SignUpForm from '../components/SignUpForm';
import LoginButtons from '../components/LoginButtons';

// Assets
// TODO: Fix warning about unknown module
import logo from '../assets/logo.svg';

/**
 * Login page.
 */
const Login = () => (
  <>
    <NoticeArea />
    <section className="hero">
      <div className="hero-body">
        <div className="container">
          <figure className="image ">
            <img src={logo} />
          </figure>
          <h1>Système de gestion de fanzine</h1>
        </div>
      </div>

      <div className="container">

        <div className="level-item has-text-centered">
          <LoginButtons />
        </div>
        <LoginForm />
      </div>
    </section>
    {/* <section className="section">
      <div className="container">
        <h1>Sign Up</h1>
        <SignUpForm />
      </div>
    </section> */}
  </>
)

export default Login;
