import React, { useEffect, useState } from "react";

//Component
import HistoryNavBar from "../components/HistoryNavBar";
import firebase from "firebase";


/**
 * Historyreciepe
 */
const HistoryReciepes = () => {

  /**
  * These are log of the commission recieve.
  */
  const [receiepeList, setReceiepeList] = useState([]);

  /**
  * Reciepes the last months.
  */
  const [monthlyReciepe, setMonthlyReciepe] = useState();
  const [monthlyReciepeLess1, setMonthlyReciepeLess1] = useState();
  const [monthlyReciepeLess2, setMonthlyReciepeLess2] = useState();

  const months = [
    "Janvier",
    "Fevrier",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre"
  ]

  const date = new Date;

  const Reciepe = ({ data }: any) => {
    const { product, author, date, reciepe } = data;
    return (
      <tr>
        <td>{product}</td>
        <td><p className="showOnMobile">Auteur : </p>{author}</td>
        <td><p className="showOnMobile">Recette : </p>{reciepe.toFixed(2)}€</td>
        <td><p className="showOnMobile">Le : </p>{date}</td>

      </tr>
    );
  }


  useEffect(() => {
    const date = new Date;

    // Load the monthly reciepe
    firebase
      .firestore()
      .collection(`reciepes`)
      .where('month', "==", date.getMonth())
      .onSnapshot(function (querySnapshot) {
        var reciepeData = 0;
        querySnapshot.forEach(function (doc) {
          console.log(doc.data().reciepe)
          reciepeData += doc.data().reciepe;
        });
        setMonthlyReciepe(reciepeData.toFixed(2))
      })

    // Load the monthly reciepe -1
    firebase
      .firestore()
      .collection(`reciepes`)
      .where('month', "==", date.getMonth() - 1)
      .onSnapshot(function (querySnapshot) {
        var reciepeData = 0;
        querySnapshot.forEach(function (doc) {
          console.log(doc.data().reciepe)
          reciepeData += doc.data().reciepe;
        });
        setMonthlyReciepeLess1(reciepeData.toFixed(2))
      })

    // Load the monthly reciepe -2
    firebase
      .firestore()
      .collection(`reciepes`)
      .where('month', "==", date.getMonth() - 2)
      .onSnapshot(function (querySnapshot) {
        var reciepeData = 0;
        querySnapshot.forEach(function (doc) {
          console.log(doc.data().reciepe)
          reciepeData += doc.data().reciepe;
        });
        setMonthlyReciepeLess2(reciepeData.toFixed(2))
      })

    // Load all ce reciepe log
    firebase
      .firestore()
      .collection(`reciepes`)
      .orderBy("timestamp", "desc")
      .get()
      .then(function (doc) {
        var logsdata = [];
        doc.forEach(element => {
          logsdata.push(element.data());
        });
        setReceiepeList(logsdata);
      })

  }, []);



  return (
    <>
      <HistoryNavBar />
      <section className="section history">
        <div className="container">
          <h1 className="subtitle dark name has-text-centered">Recette en commission</h1>
          <nav className="level box">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth()]} </p>
                <p className="title">{monthlyReciepe}€</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth() - 1]} </p>
                <p className="title">{monthlyReciepeLess1}€</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">{months[date.getMonth() - 2]} </p>
                <p className="title">{monthlyReciepeLess2}€</p>
              </div>
            </div>
          </nav>
          <table>
            <tr>
              <th className="hideOnMobile">Fanzine : </th>
              <th className="hideOnMobile">Auteur : </th>
              <th className="hideOnMobile">Recette : </th>
              <th className="hideOnMobile">Date et heure : </th>
            </tr>
            <tbody>
              {receiepeList.map((item: {}, k: number) => {
                return typeof item !== "undefined" ? (
                  <Reciepe data={item} key={k} />
                ) : null
              })}
            </tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default HistoryReciepes;