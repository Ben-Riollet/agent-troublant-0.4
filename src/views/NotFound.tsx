import React from 'react';

// 404 page
// TODO: Add some custom style
const NotFound = () => {
  return (
    <>
      <section className="section">
        <div className="container">
          <p className="sub-title">Vous vous êtes égaré..</p>
          <img src="https://firebasestorage.googleapis.com/v0/b/agent-troublant.appspot.com/o/lonely.jpg?alt=media&token=b6b58651-4cc0-4d6f-b75f-8ba59e66787f" />
<br/>
          <a href="/" className="button">Je veux rentrer à la maison</a>
        </div>
      </section>
    </>
  )
}

  
)

export default NotFound;
