import React, { useEffect, useState } from "react";

//Data
import CardList from "../components/Card";
import HistoryNavBar from "../components/HistoryNavBar";
import { loadRefundLog } from "../api/History";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoneyBillAlt, faHandHoldingUsd } from "@fortawesome/free-solid-svg-icons";


/**
 * History of the last Recieve
 */
const HistoryLastRefund = () => {
    /**
     * These are the fanzine linked to the profil.
     */
    const [refundLogs, setRefundLogs] = useState([]);

    const Refund = ({ data }: any) => {
      const { date, name, path, soldGiven } = data;
      return (
        <tr>
          <td><a href={`/profil/${path}`}>{name}</a></td>
          <td><p className="showOnMobile">Rendu : </p>{soldGiven}€</td>
          <td><p className="showOnMobile">Le : </p>{date}</td>
        </tr>
      );
    }

    useEffect(() => {
        // Get recieve logs.
      var logsLoaded = loadRefundLog()
      logsLoaded
        .then(function (doc) {
          var logsdata = [];
          doc.forEach(element => {
            logsdata.push(element.data());
          });
          setRefundLogs(logsdata);
        })
    }, []);

    return (
        <>
            <HistoryNavBar />
            <section className="section history">
                <div className="container">
                <h1 className="subtitle dark name">Dernier remboursement <span className="icon">
            <FontAwesomeIcon icon={faHandHoldingUsd} />
          </span></h1>
                <table>
                        <tr>
                            <th className="hideOnMobile">Fanzine</th>
                            <th className="hideOnMobile">Argent rendu</th>
                            <th className="hideOnMobile">Date et heure</th>
                        </tr>
                        <tbody>
                            {refundLogs.map((item: {}, k: number) => {
                                return typeof item !== "undefined" ? (
                                    <Refund data={item} key={k} />
                                ) : null
                            })}
                        </tbody>
                    </table>
                </div>
            </section>
        </>
    );
};

export default HistoryLastRefund;

