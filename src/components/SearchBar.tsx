import React, { useEffect, useState } from 'react';
import Autosuggest from 'react-autosuggest';
import Truncate from 'react-truncate';


//Data
import { getHomeCards } from '../api/Card';

const SearchBar = (filtering) => {

  //Data of the cards completion
  const [items, setItems] = useState([]);

  /**
     * AUTOSUGGEST for homepage
     */

  function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    const regex = new RegExp('^' + escapedValue, 'i');
     //items.filter(items => regex.test(items.name));
     console.log(items)
    filtering(items.filter(items => regex.test(items.name));)
    return items.filter(items => regex.test(items.name));
  }

  function getSuggestionValue(suggestion) {
    return suggestion.name;
  }

  function renderSuggestion(suggestion) {
    if (suggestion.type == 'profile') {
      return (
        <div className="box profile">
          <a href={`profil/${suggestion.profile_path}`}>
            <article className="media">
              <div className="media-content">
                <div className="content">
                  <h1 className="title">{suggestion.name.toUpperCase()}</h1>
                  <Truncate lines={2} ellipsis={<span>...</span>}>
                    {suggestion.description}
                  </Truncate>
                </div>
              </div>
            </article>
          </a>
        </div >
      );
    } else {
      return (
        <div className="box product">
          <a href={`fanzine/${suggestion.product_path}`}>
            <p className="sub-title">Fanzine</p>
            <article className="media">
              <div className="media-left">
                <figure className="image  ">
                  <img src={suggestion.image ? suggestion.image : "http://placekitten.com/640/260"} alt={suggestion.name ? suggestion.name : "avatar"} />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong className="title">{suggestion.name}</strong>
                    <br />
                    {(suggestion.price == 0) ? "Prix libre" : suggestion.price + "€"}
                    <br />
                    {suggestion.stock} pce(s)
              </p>
                </div>
              </div>
            </article>
            <p>Par <strong>{suggestion.author}</strong></p>
          </a>
        </div >
      )
    }
    return <></>;
  }

  class AutoSuggestSearch extends React.Component {
    constructor() {
      super();

      this.state = {
        value: '',
        suggestions: []
      };
    }

    onChange = (event, { newValue, method }) => {
      this.setState({
        value: newValue
      });
    };

    onSuggestionsFetchRequested = ({ value }) => {
      this.setState({
        suggestions: getSuggestions(value)
      });
    };

    onSuggestionsClearRequested = () => {
      this.setState({
        suggestions: []
      });
    };

    handleScroll() {
      var header = document.getElementById("searchSuggestion");
      var searchresult = document.getElementById("react-autowhatever-1");
      var x = document.body.scrollTop || document.documentElement.scrollTop;

      if (header && searchresult && x > 52) {
        header.classList.add("sticky");
        searchresult.classList.add("sticky");
      } else if (header && searchresult && x < 52) {
        header.classList.remove("sticky");
        searchresult.classList.add("sticky");
      }
    }

    componentDidMount() {
      window.addEventListener('scroll', this.handleScroll);
    }

    render() {
      const { value, suggestions } = this.state;
      const inputProps = {
        placeholder: "rechercher :",
        value,
        onChange: this.onChange,
        id: "searchSuggestion",
        className: "input is-large"
      };


      return (
        <>
          <div className="search-bar">
            <h1 className="search-bar title">pokédex de fanzines : </h1>
            <Autosuggest
              suggestions={suggestions}
              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
              getSuggestionValue={getSuggestionValue}
              renderSuggestion={renderSuggestion}
              inputProps={inputProps}
            />
          </div>
        </>
      );
    }
  }

  // On component load, load its data from firestore.
  useEffect(() => {
    getHomeCards().then(cards => setItems(cards))
  }, []);

  // Finally, render it!
  return (
    <>
      <div className="field">
        <div className="control" >
          <AutoSuggestSearch />
        </div>
      </div>
    </>
  )

}


export default SearchBar;
