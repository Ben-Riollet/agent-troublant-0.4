import React, { useEffect } from "react";
import { useState } from "react";
import imageCompression from 'browser-image-compression';
import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

/**
 * Upload
 * Manage preview,  and handleChange.
 * TODO : inplemente canavs and exit npm module to reorientated photo from phone
 * 
 * @param param0 
 * CallBack handleChange.
 */
const Upload = ({ action, oldImage, storagePath }: any) => {

    // If we are in edit case, show the old image.
    const [image, setImage] = useState({
        file: oldImage
    });

    /**
     * Get, compress and send to the view the file.
     */
    function handleImageUpload(event) {

        //Get the file
        var imageFile = event.target.files[0];
        console.log("imageFile", imageFile)
        console.log('originalFile instanceof Blob', imageFile instanceof Blob); // true
        console.log(`originalFile size ${imageFile.size / 1024 / 1024} MB`);

        // Set parameter of compression
        var options = {
            maxSizeMB: 0.1,
            maxWidthOrHeight: 300,
            useWebWorker: true
        }

        // Compress the image
        imageCompression(imageFile, options)
            // Upload the file
            .then(function (compressedFile) {
                console.log('compressedFile instanceof Blob', compressedFile instanceof Blob); // true
                console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
                console.log("compressedFile", compressedFile)
                return action(compressedFile, storagePath);
            })
            // Remove the last uploaded file
            .then(() => {
                if (document.getElementsByClassName('uploadedImage')) {
                    var oldImages = document.getElementsByClassName('uploadedImage');
                    while (oldImages[0]) {
                        oldImages[0].parentNode.removeChild(oldImages[0]);
                    }
                }
            })
            // Set the preview
            .then(() => {
                const img = document.createElement("img");
                const preview = document.getElementById('preview')
                img.classList.add("uploadedImage");
                img.file = imageFile;
                preview.appendChild(img);
                const reader = new FileReader();
                reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
                reader.readAsDataURL(imageFile);
            })
            .catch(function (error) {
                console.log(error.message);
            });
    }

    useEffect(() => {
        if (oldImage) {
            setImage({
                file: oldImage
            });
        }
    }, []);

    return (
        <>
            <section className="hero">
                <div className="container" id="preview">
                    <img id="image" className="uploadedImage" src={image.file ? image.file : (oldImage ? oldImage : "http://placekitten.com/650/261")} />
                </div>
                <div className="file">
                    <label className="file-label">
                        <input
                            id="file-input"
                            className="file-input"
                            type="file"
                            name="image"
                            onChange={handleImageUpload}
                        />
                        <span className="file-cta">
                            <span className="file-icon">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faUpload} />
                                </span>
                            </span>
                            <span className="file-label">Chercher ...</span>
                        </span>
                    </label>
                </div>
            </section>
        </>
    );

}
export default Upload;