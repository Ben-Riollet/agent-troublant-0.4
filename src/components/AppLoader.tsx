import React, { FunctionComponent, useState, useEffect } from 'react';

// API
import { isAuthenticated } from '../api/Auth';

// Assets
import './AppLoader.scss';

// The loading message
// Source: https://tobiasahlin.com/spinkit/
const Loading = () => (
  <div className="sk-folding-cube">
    <div className="sk-cube1 sk-cube"></div>
    <div className="sk-cube2 sk-cube"></div>
    <div className="sk-cube4 sk-cube"></div>
    <div className="sk-cube3 sk-cube"></div>
  </div>
)

// The AppLoader ensure that the application is fully ready once we access it.
// Once the "ready" value is TRUE, renders the component children.
// On component mount, check if the user status is ready to be fetched.
const AppLoader: FunctionComponent = props => {

  const [ready, setReady] = useState(false);

  useEffect(() => {
    isAuthenticated(() => setReady(true));
  }, [])

  return ready ? <>{props.children}</> : <Loading />
}

export default AppLoader;
