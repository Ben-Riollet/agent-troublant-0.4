import React, { useContext } from 'react';
import AppContext from './AppContext';
import { useSpring, animated } from 'react-spring';

/**
 * Available notice levels.
 * The selected item will be treated as additional class.
 */
export enum NoticeLevel {
  Success = 'is-success',
  Warn = 'is-warn',
  Error = 'is-danger',
}

/**
 * Notice struct.
 */
export interface NoticeInterface {
  body: string;
  level?: NoticeLevel;
}

/**
 * Notice.
 * Renders a message with an optional style (level).
 */
export const Notice = ({ notice }: { notice: NoticeInterface }) => {

  const { removeNotice } = useContext(AppContext);
  const { body, level } = notice;

  const fadeIn = useSpring({
    from: {
      opacity: 0
    },
    to: {
      opacity: 1
    }
  });

  return (
    <animated.div style={fadeIn}>
      <div className={`notification ${level}`}  onClick={() => removeNotice(notice)}>
        {body}
      </div>
    </animated.div>
  );
}

/**
 * List of notice messages.
 * TODO: Get notices from the props?
 */
export const NoticeArea = () => {

  const { notices } = useContext(AppContext);

  return (
    <>
      {notices.map((notice: NoticeInterface, k: number) => {
        return <Notice notice={notice} key={k} />
      })}
    </>
  );
}
