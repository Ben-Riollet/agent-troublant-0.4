import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faHandHoldingUsd, faShieldAlt, faMoneyBillWave, faTruck } from '@fortawesome/free-solid-svg-icons';
import { Link } from "react-router-dom";
import firebase from "firebase";
import logo from '../assets/logo.svg';



/**
 * HistoryNavBar
 */
const HistoryNavBar = ({ history }: any) => {

    /**
   * The full dept
   */
    const [dept, setDept] = useState();

    /**
     * If true the menu is open.
     */
    const [open, setOpen] = useState(false);

    /**
     * Toggle Menu by clicking on the burger.
     */
    const toggleMenu = () => {
        setOpen(!open);
    };

    useEffect(() => {
        // The full dept.
        firebase
            .firestore()
            .collection(`history`)
            .where('currentDept', ">=", 0)
            .onSnapshot(function (querySnapshot) {
                var deptData = 0;
                querySnapshot.forEach(function (doc) {
                    deptData += doc.data().currentDept;
                });
                setDept(deptData);
            })
    }, []);

    return (
        <nav className="navbar navbar is-fixed-top" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <div className="navbar-start">
                    <div className="navbar-item">
                        <div className="buttons">
                            <Link to="/" className="button is-light" onClick={() => history.goBack()}>
                                <span className="icon fa-arrow-left">
                                    <FontAwesomeIcon icon={faArrowLeft} />
                                </span>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="navbar-item">
            <Link to={`/`}>
              <figure className="image ">
                <img src={logo} />
              </figure>
            </Link>
          </div>
                <a
                    role="button"
                    className={`navbar-burger burger ${open ? "is-active" : ""}`}
                    aria-label="menu"
                    aria-expanded={open}
                    onClick={toggleMenu}
                >
                    <span aria-hidden="true" />
                    <span aria-hidden="true" />
                    <span aria-hidden="true" />
                </a>
            </div>
            <div className={`navbar-menu ${open ? "is-active" : ""}`}>
                <div className="navbar-end">
                    <div className="navbar-item">
                        <Link to={`/historique/ventes`} className="buttons" >
                            <div className="button is-light">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faMoneyBillWave} />
                                </span>
                                <span>Ventes</span>
                            </div>
                        </Link>
                    </div>
                    <div className="navbar-item" >
                        <Link to={`/historique/livraisons`} className="buttons">
                            <div className="button is-light">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faTruck} />
                                </span>
                                <span>Livraison</span>
                            </div>
                        </Link>
                    </div>
                    <div className="navbar-item" >
                        <Link to={`/historique/remboursements`} className="buttons" >
                            <div className="button is-light">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faHandHoldingUsd} />
                                </span>
                                <span>Remboursements</span>
                            </div>
                        </Link>
                    </div>
                    <div className="navbar-item">
                        <Link to={`/historique/liste-dettes`} className="buttons" >
                            <div className="button is-light">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faShieldAlt} />
                                </span>
                                <span>Dette générale {dept}€</span>
                            </div>
                        </Link>
                    </div>
                    <div className="navbar-item">
                        <Link to={`/historique/recettes`} className="buttons" >
                            <div className="button is-light">
                                <span className="icon">
                                    <FontAwesomeIcon icon={faShieldAlt} />
                                </span>
                                <span>Recettes</span>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </nav>
    )
}
export default HistoryNavBar;