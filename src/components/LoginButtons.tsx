import React, { useContext } from 'react';
import { withRouter } from 'react-router';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faGoogle, faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';

// Components
import AppContext from './AppContext';
import { signInWithGoogle, signInWithFacebook } from '../api/Auth';

// Social login button (OAuth).
const SocialLoginButton = ({ title, icon, onClick }: { title: string, icon: IconProp, onClick?: any }) => (
  <button className="button" onClick={onClick}>
    <span className="icon">
      <FontAwesomeIcon icon={icon} />
    </span>
    <span>{title}</span>
  </button>
);

// List of social login buttons
const LoginButtons = ({ history }: any) => {

  // Main controller.
  const { addNotice } = useContext(AppContext);

  // Google
  const handleGoogleLogin = () => {
    signInWithGoogle()
      .then(() => {
        addNotice('You are now connected using Google!', 'is-success');
        history.push('/');
      })
      .catch((err: Error) => addNotice(err.message, 'is-danger'));
  }

  // Facebook
  const handleFacebookLogin = () => {
    signInWithFacebook()
      .then(() => {
        addNotice('You are now connected using Facebook!', 'is-success');
        history.push('/');
      })
      .catch((err: Error) => addNotice(err.message, 'is-danger'));
  }

  return (
    <p className="buttons">
      <SocialLoginButton title="Google" icon={faGoogle} onClick={handleGoogleLogin} />
      <SocialLoginButton title="Facebook" icon={faFacebookF} onClick={handleFacebookLogin} />
    </p>
  );
}

export default withRouter(LoginButtons);
