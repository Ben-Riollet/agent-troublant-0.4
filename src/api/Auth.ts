import firebase from 'firebase/app';

/**
 * Get the current user.
 * Returns the authenticated user on this device.
 * Returns NULL if the device is not authenticated.
 */
export function getCurrentUser() {
  return firebase.auth().currentUser;
}

// Whenever the authentication state of the user has changed.
// If the user is authenticated, returns its object.
// If the user is not authenticated, returns NULL.
export async function isAuthenticated(nextOrObserver: (user: firebase.User | null) => any) {
  return await firebase.auth()
    .onAuthStateChanged(nextOrObserver);
}

/**
 * Sign in using email and password.
 * @param email
 * @param password
 */
export async function signIn(email: string, password: string) {
  return await firebase.auth()
    .signInWithEmailAndPassword(email, password)
}

// Sign in using Google.
export async function signInWithGoogle() {
  var provider = new firebase.auth.GoogleAuthProvider();
  return firebase.auth().signInWithPopup(provider);
}

// Sign in using Facebook.
export async function signInWithFacebook() {
  var provider = new firebase.auth.FacebookAuthProvider();
  return firebase.auth().signInWithPopup(provider);
}

// Log Out function.
export async function logOut() {
  return await firebase.auth()
    .signOut();
}
