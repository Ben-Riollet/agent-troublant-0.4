/**
 * Interface for the log
 */
export interface LogInterface {
  message: string; //Descripe the log
  typeLog: string; //add, create, edit, delete, refund, sell, reception
  date: string; //The date of the log
  timestamp: number;
  name: string; //Name of the element concerne
  typeElement: string; //product, profile
  path?: string; //Path of the élement concerne. Except in delete
  unities?: string; //For sell ans recieve
  money?: string; //For sell, refunfing
  priceSold?: number; //If freeprice we get the price
}
