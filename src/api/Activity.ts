/**
 * Activities
 * TODO: Everything
 */

/**
 * Type the lastActivities model for listCard.
 */
export interface ActivityInterface {
  operation: string;
  date: string;
  product?: string;
  quantity?: string;
  price?: string;
}


