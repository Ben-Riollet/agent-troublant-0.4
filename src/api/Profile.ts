import firebase from "firebase/app";
import { getCurrentUser } from "./Auth";
import { redirect } from "./Utilities";
//When a use require it works, not with import.
const urlSlug = require("url-slug");

/**
 * PROFILE
 * TODO: Catch original promises in depth to send back a proper ProfileInterface***********???
 */

/**
 * Profile interface.
 */
export interface ProfileInterface {
  name: string;
  description?: string;
  activities?: string;
  creationDate: string;
  location?: string;
  phone?: string;
  email?: string;
  siret?: string;
  wallet?: number;
  path: string;
}

/**
 * Profile card interface.
 */
export interface ProfileCardInterface {
  description?: string;
  timestamp: number;
  name: string;
  type?: string;
  profile_path: string;
}


/**
 * Get the current user profile
 * TODO: Send a promise with an error if not authenticated
 * TODO: Why there is a probleme with : uid ?
 */
export async function getCurrentUserProfile() {
  const currentUser = getCurrentUser();
  const uuid = currentUser ? currentUser.uid : null;

  return await firebase
    .firestore()
    .collection("profiles")
    .where("uuid", "==", uuid)
    .get()
    .then(function() {
      console.log("Getting curent profile succeed");
    })
    .catch(function(error) {
      console.error("Error getting curent profile : ", error);
    });
}

/**
 * Update profile data.
 * @param id
 * @param profile
 */
export async function updateProfile(id: string, profile: ProfileInterface) {
  return await firebase
    .firestore()
    .collection("profiles")
    .doc(id)
    .set(profile, { merge: true })
    .then(function() {
      redirect(`profile/${profile.name.toLowerCase().replace("/s/g", "-")}`);
    })
    .then(function() {
      //***********************************************************How can i convert interface ? */
      //updateCard(id, profile)
      console.log("Document successfully updated!");
    })
    .catch(function(error) {
      console.error("Error updating : ", error);
    });
}

//Create Profile
/*
 * ******************************************************************Sould I move the operation in function/src/index.tsx ?
 */
export async function createProfile(profile: ProfileInterface) {
  var siret: string = profile.siret ? profile.siret : " ";
  var description: string = profile.description ? profile.description : " ";
  var location: string = profile.location ? profile.location : " ";
  var phone: string = profile.phone ? profile.phone : " ";

  await firebase
    .firestore()
    .collection("profiles")
    .doc(profile.name.toLowerCase().replace("/s/g", "-"))
    .set({
      creationDate: new Date(),
      description: description,
      location: location,
      name: profile.name,
      email: profile.email,
      phone: phone,
      siret: siret
    })
    .then(function() {
      console.log("Document successfully written!");
    })
    .catch(function(error) {
      console.error("Error writing document: ", error);
    });

  /*
   *Edit the cards linked
   */
  /*******************************************************Do really have to change the id for the cards table ?  */
  /*******************************************************MMh... If one day, i want to change the composition of the card it's not gonna be easy non?  */
  await firebase
    .firestore()
    .collection("cards")
    .doc(profile.name.toLowerCase().replace("/s/g", "-"))
    .set({
      description: profile.description,
      name: profile.name,
      categories: "profile",
      profile_id: profile.name.toLowerCase().replace("/s/g", "-")
    })
    .then(function() {
      redirect(`profile/${profile.name.toLowerCase().replace("/s/g", "-")}`);
    })
    .then(function() {
      console.log("Cards successfully written!");
    })
    .catch(function(error) {
      console.error("Error writing cards: ", error);
    });
}



/**
 * Delete a profile.
 * TODO: Do we delete the user at the same time ?
 * @param id
 */
export async function deleteAProfile(id: string) {
  return await firebase
    .firestore()
    .collection("profiles")
    .doc(id)
    .delete();
}

/**
 * Process the moneyBack action
 * @param id
 */
export async function moneyGivenBack(id: string) {
  return await firebase
    .firestore()
    .collection("profiles")
    .doc(id)
    .set(
      {
        wallet: 0
      },
      { merge: true }
    );
}

/**
 * Load one product data.
 * @param id
 */
export async function loadProfile(id: string) {
  return await firebase
    .firestore()
    .collection(`profiles`)
    .doc(id)
    .get()
    .then(snapshot => {
      return snapshot.data();
    })
    .catch(err => {
      console.log("Error => ", err);
    });
}

/**
 * Load one product data.
 * @param id
 */
export async function loadProfileCreation(id: string) {
  return new Promise((resolve, reject) => {
  firebase
    .firestore()
    .collection(`cards`)
    .where("path_author", "==", id)
    .get()
    .then(qs => {
      let cards = qs.docs.map(doc => doc.data());
      return resolve(cards);
    })
    .catch(err => reject(err));
  });
}


/**
 * Edit one profile data.
 * @param id
 */
export async function editProfile(id: string, data: []) {
  return await firebase
    .firestore()
    .collection("profiles")
    .doc(id)
    .set(data, { merge: true })
    .catch(err => {
      console.log("Error => ", err);
    });
}

/**
 * Save profile on create.
 * @param id
 */
export async function saveProfile(data: any) {
  const date = new Date();
  return await firebase
  .firestore()
  .collection("profiles")
  .doc(urlSlug(data.name))
  .set({
    date: date.getDate() +
      "/" +
      ("0" + (date.getMonth() + 1)).slice(-2) +
      "/" +
      date.getFullYear() +
      "  " +
      ("0" + date.getHours()).slice(-2) +
      ":" +
      ("0" + date.getMinutes()).slice(-2),
    timestamp: Date.now(),
    description: data.description || '',
    activities: data.activities || '',
    location: data.location || '',
    name: data.name,
    email: data.email || 'A renseigner',
    phone: data.phone || 'A renseigner',
    siret: data.siret || '',
    wallet: 0,
    path: urlSlug(data.name)
  })
    .catch(err => {
      console.log("Error => ", err);
    });
}