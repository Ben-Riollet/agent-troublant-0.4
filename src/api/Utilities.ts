/**
 * UTILITIES
 * TODO: Utiliser react router et supprimer ce fichier
 */

 /**
  * Permit to redirect when a action is done.
  * @param param
  */
export function redirect(param?: string) {
    if(param){
      window.location.pathname = `/${param}`
    }
    else{
      window.location.pathname = `/`
    }
  }
