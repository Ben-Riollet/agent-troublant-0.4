'use strict';

//CREATION FEATURES

/**
 * Create profile when user is created
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'createProfile') {
  exports.createProfile = require('./createFeature').createProfile;
}

/**
 * When created a profil we create a history dept document.
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'createProfileAuthor') {
  exports.createProfileAuthor = require('./createFeature').createProfileAuthor;
}

/**
 * When created a product  we create card and log
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'createProduct') {
  exports.createProduct = require('./createFeature').createProduct;
}


//UPDATE FEATURES
/**
 * Update database when profil has been modified.
 * TODO: Create a log for refund)
 * TODO: update the history money win for Agent
 */

 /**
 * When edit or solde mouvement, update the data's profile.
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'updateProfile') {
  exports.updateProfile = require('./updateFeature').updateProfile;
}

/**
 * When edit, sell, recieve product, update the data.
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'updateProduct') {
  exports.updateProduct = require('./updateFeature').updateProduct;
}


//DELETE FEATURES

/**
 * Manage deletion of product
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'deleteProduct') {
  exports.deleteProduct = require('./deleteFeature').deleteProduct;
}

/**
 * Manage deletion of profile
 */
if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'deleteProfile') {
  exports.deleteProfile = require('./deleteFeature').deleteProfile;
}
